# PyNAO

PyNAO the Python Numerical Atomic Orbitals.

PyNAO is designed to perform efficient many-body perturbation theory methods 
with numerical atomic orbitals. Actual implemented methods are

* TDDFT (light and electronic perturbation, i.e., EELS)
* G0W0 (GW is ready ??)
* BSE

The code needs inputs from previous DFT calculations. Supported DFT codes are

* [Siesta](https://departments.icmab.es/leem/siesta/)
* [PySCF](https://sunqm.github.io/pyscf/)

For more details, refer to the project [webpage](https://mbarbry.website.fr.to/pynao/doc/html).

## Installation

    apt-get update
    apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
    pip3 install -r requirements.txt
    export CC=gcc && export FC=gfortran && export CXX=g++
    cp lib/cmake_user_inc_examples/cmake.user.inc-gnu lib/cmake.arch.inc
    python setup.py bdist_wheel
    pip install dist/pynao-0.1-py3-none-any.whl

Furtrher details can be found on the installation instruction 
[page](https://mbarbry.website.fr.to/pynao/doc/install.html).

## Bug report

* Barbry Marc <marc.barbry@mailoo.org>
* Koval Peter <koval.peter@gmail.com>
