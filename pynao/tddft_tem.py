from __future__ import print_function, division
import numpy as np
from timeit import default_timer as timer
from pynao import tddft_iter
from pynao.m_siesta_units import siesta_conv_coefficients
from pynao.nao import nao

class tddft_tem(tddft_iter):
    """tddft_tem object.

    Iterative TDDFT using the electostatic potential of a moving charge as
    perturbation. The units of the input are in atomic units.

    The theory behind this implementation and some application can be found
    in Ref [1].

    Input Parameters:

    dr: 1D np.array, float, size 3
        spatial resolution for the electron trajectory in atomic unit.
        Warning: This parameter influence the accuracy of the calculations.
        if it is taken too large the results will be wrong.
    freq: 1D np.array, float
        Frequency range (in atomic unit), freq[0] must be 0.0!!

    References:

    1. M. Barbry, “Plasmons in nanoparticles: Atomistic ab initio theory
       for large systems, Ph.D. thesis, University of Basque Country, 
       Donostia-San Sebastián, Spain, 2018,
       http://cfm.ehu.es/view/files/MArc_barbry_2-1.pdf
    """
 
    def __init__(self, **kw):
       

        self.kw = kw
        self.verbosity = kw["verbosity"] if "verbosity" in kw else 0
        self.freq = kw["freq"] if "freq" in kw else np.arange(0.0, 0.367, 1.5*self.eps)
        self.dr = kw["dr"] if "dr" in kw else np.array([0.3, 0.3, 0.3])
        self.initialiazed = False
        self.V_freq = None
        self.velec = None
        self.beam_offset = None

        # First element of freq must be 0.0 for the Fourier transform
        assert abs(self.freq[0]) < 1.0e-12

    def get_EELS_spectrum(self, velec=np.array([1.0, 0.0, 0.0]), 
                          beam_offset=np.array([0.0, 0.0, 0.0]),
                          tmp_fname=None, calc_Vext=True, inter=True):
        """
        Calculate the interacting TEM spectra for a given electron trajectory.

        Input Parameters:

        velec: 1D np.array, float
               xyz component of the electron velocity in atomic unit
        beam_offset: 1D np.array, float
                     xyz components of the beam offset, must be orthogonal
                     to velec in atomic unit
        tmp_fname: string
                   filename to store temporary results of the EELS spectra.
                   if None, the results will not be saved during the run
        calc_Vext: bool
                   Calculate Vext or not. If Vext has been previously calculated,
                   it is not necessary to calculate it again
        inter: bool
               Perform interactive or non-interactive calculations.

        The velocity is given in atomic units, it can be converted from energy
        (in Hartree) using the relativistic energy kinetic formulation.
        Below is a function doing such conversion

        Convert electron energy to velocity:

        >>> def E2vel(E):
        >>>     # Fine-structure
        >>>     alpha = 0.0072973525693
        >>>     num = np.sqrt((alpha**2)*E**2 + 2*E)
        >>>     den = E*alpha**2 + 1
        >>>     return num/den
        >>> # Convert electron energy in eV to velocity in atomic unit
        >>> Ha = 27.211
        >>> E = 1e5/Ha      # E = 100 keV
        >>> v = E2vel(E)
        >>> print(v)
        """

        assert velec.size == 3
        assert beam_offset.size == 3
        if tmp_fname is not None:
            if not isinstance(tmp_fname, str):
                raise ValueError("tmp_fname must be a string")

        if not calc_Vext and any(self.velec != velec):
            calc_Vext = True
        self.velec = velec

        if not calc_Vext and any(self.beam_offset != beam_offset):
            calc_Vext = True
        self.beam_offset = beam_offset

        self.vnorm = np.sqrt(np.dot(self.velec, self.velec))
        self.vdir = self.velec/self.vnorm

        # In order to get atom2coord, we need nao constructor
        # It repeat twice with tddft_iter, but nao init should be fast,
        # in contrary to tddft_iter
        nao_const = nao(**self.kw)
        self.check_collision(nao_const.atom2coord)
        if not self.initialiazed:
            tddft_iter.__init__(self, **self.kw)
            self.initialiazed = True

        self.get_time_range()
        if calc_Vext:
            self.calc_external_potential()
        else:
            if self.V_freq is None:
                print("self.V_freq is None")
                self.calc_external_potential()

        self.dn, gamma = self.comp_tem_spectrum(self.V_freq, tmp_fname=tmp_fname,
                                                inter=inter)
        return gamma

    def check_collision(self, atom2coord, threshold=1.0e-6):
        """
        Check if the electron collide with an atom
        if the trajectory of the electron is passing too close of an atom, i.e.,
        if at any moment in time the distance between the electron and an atom
        is below the value given by threshold.

        Also check that the electron velocity and the beam ofset are perpendicular.
        
        Input Parameters:

        atom2coord: 2D np.array, float
                    Atoms coordinates
        threshold: float
                   Threshold use to determine if the beam is colliding an atom.
        """

        if self.verbosity>0:
            print("{} => tem parameters:".format(__name__))
            print("vdir: ", self.vdir)
            print("vnorm: ", self.vnorm)
            print("beam_offset: ", self.beam_offset)

        # check orthogonality between beam direction
        # and beam offset
        assert abs(np.dot(self.velec, self.beam_offset)) < threshold
 
        R0 = -100.0*np.max(atom2coord)*self.vdir + self.beam_offset

        for atm in range(atom2coord.shape[0]):
            vec = R0 - atom2coord[atm, :]
            
            # unit vector to compare to vdir
            vec = abs(vec/np.sqrt(np.dot(vec, vec)))

            if np.sqrt(np.dot(vec-self.vdir, vec-self.vdir)) < threshold:
              ######### fancy message does not work in python2
              mess = 'np.sqrt(np.dot(vec-self.vdir, vec-self.vdir))<1e-6:'
              print("atoms {0} coordinate: ".format(atm), atom2coord[atm, :])
              #mess = """
              #Electron is collinding with atom {0}:
              #velec = [{1:.3f}, {2:.3f}, {3:.3f}]
              #beam_offset = [{4:.3f}, {5:.3f}, {6:.3f}]
              #atom coord = [{7:.3f}, {8:.3f}, {9:.3f}]
              #impact parameter = {10:.9f} > 1e-6""".format(atm, *self.velec,
              #                                             *self.beam_offset[0],
              #                                             *atom2coord[atm, :],
              #                                             np.sqrt(np.dot(vec, self.vdir)))
              raise ValueError(mess)

    def get_time_range(self):
        """
        Get the time and symmetric frequency range for the electron passing close
        to the particle. The tim e range is a symmetric array
        around 0.0. At t = 0, the electron is at its closest
        position from the molecule. This array will depend on the
        frequency range and the spatial precision dr.
        To respect the Fourier transform convention, the following
        relationshiip must be fulfill,

        N = 2*pi/(dw*dt)

        with N the number of element of t.
        N must be an odd number in order that t is symmetric
        """
        from pynao.m_tools import is_power2

        dt = np.min(self.dr)/self.vnorm
        dw = self.freq[1] - self.freq[0]

        N_org = int(2*np.pi/(dw*dt))

        # to improve performance, N must be a power of 2
        if not is_power2(N_org):
            power = 1
            while 2**power < N_org:
                power +=1

            minima = np.argmin(np.array([abs(2**(power-1) - N_org), abs(2**power - N_org)]))
            if minima == 0:
                N = 2**(power-1)
            else:
                N = 2**power

            if self.verbosity>0: print("N_org = {0}, N_new = {1}".format(N_org, N))
            dt = 2*np.pi/(N*dw)
            dr = dt*self.vnorm
            self.dr = np.array([dr, dr, dr])
        else:
            N = N_org

        dw_symm = 2.0*np.pi/(N*dt)
        wmax = 2.0*np.pi*(N-1)/(N*dt)/2.0
        self.freq_symm = np.arange(-wmax, wmax+dw_symm, dw_symm)[0:N]

        tmax = (N-1)*dt/2
        self.time = np.arange(-tmax, tmax+dt, dt)[0:N]
        
    def calc_external_potential(self):
        """
        Calculate the external potential created by a moving charge
        """
        from pynao.m_comp_vext_tem import comp_vext_tem

        t1 = timer()
        self.V_freq = comp_vext_tem(self, self.pb.prod_log, self.numba_parallel)
        t2 = timer()
        if self.verbosity > 2:
            print("{} => calc_external_potential timing: {}".format(__name__, t2-t1))
            print("sum(V_freq) = ", np.sum(abs(self.V_freq.real)),
                  np.sum(abs(self.V_freq.imag)))

    def comp_tem_spectrum(self, V_freq, tmp_fname=None, inter=True):
        """ 
        Compute the interacting tem spectrum

        Input Parameters:

        comegas: 1D np.array, complex
                 frequency range (in Hartree) for which the polarizability is computed.
                 The imaginary part control the width of the signal.
                 For example,
                 >>> td = tddft_iter_c(...)
                 >>> comegas = np.arange(0.0, 10.05, 0.05) + 1j*td.eps

        V_freq: 2D np.array, complex
                The perturbation to the system

        inter: boolean
               Perform TDDFT calculation without or with interaction
        tmp_fname: string
                   temporary file to save polarizability at each frequency.
                   Can be a life saver for large systems.
                   The format of the file is the following,
                   # energy (Hartree)    Re(gamma)    Im(gamma)

        Output Parameters:

        dn: 2D np.array, complex
            computed density change in prod basis
        gamma: 1D np.array, complex
               computed eels spectrum
        """
        comegas = self.freq + 1.0j*self.eps
        gamma = np.zeros_like(comegas, dtype=self.dtypeComplex)
        dn = np.zeros((comegas.shape[0], self.nprod), dtype=self.dtypeComplex)

        for iw, comega in enumerate(comegas):

            chi0mv_ncalls_ite = self.rf0_ncalls
            t1 = timer()
            if inter:
                veff = self.comp_veff(V_freq[iw, :], comega, x0=None)

                dn[iw, :] = self.apply_rf0(veff, comega, self.chi0_mv)
                gamma[iw] = np.dot(np.conj(V_freq[iw, :]), dn[iw, :])
            else:
                dn[iw, :] = self.apply_rf0(V_freq[iw, :], comega, self.chi0_mv)
                gamma[iw] = np.dot(dn[iw, :], np.conj(V_freq[iw, :]))
            t2 = timer()


            chi0mv_ncalls_ite = self.rf0_ncalls - chi0mv_ncalls_ite
            if self.verbosity > 1:
                mess = "iw: {0}/{1}; w: {2:.4f};".format(iw, comegas.shape[0],
                                                         comega.real*siesta_conv_coefficients["ha2ev"])
                mess += " nite: {0}; time: {1}".format(chi0mv_ncalls_ite, t2-t1)
                print(mess)

            if tmp_fname is not None:
                tmp = open(tmp_fname, "a")
                tmp.write("{0}   {1}   {2}\n".format(comega.real, -gamma[iw].real/np.pi, 
                                                                  -gamma[iw].imag/np.pi))
                tmp.close() 
                # Need to open and close the file at every freq, otherwise
                # tmp is written only at the end of the calculations, therefore,
                # it is useless

        return dn, -gamma/np.pi
