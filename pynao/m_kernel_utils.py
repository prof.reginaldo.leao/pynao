import numpy as np

def kernel_initialization(self, **kw):
    """
    Load the kernel from file or compute it

    Inputs:

        self: a scf (scf.py) or tddft_iter (tddft_iter.py) class
        xc_code
        keywords of comp_fxc_pack method
        load_kernel (bool, optional)
        keywords of load_kernel (optional)
    """

    if "xc_code" not in kw.keys():
        raise ValueError("xc_code not in kw")
    load = kw["load_kernel"] if "load_kernel" in kw else False 
    xc_code = kw["xc_code"]

    xc = xc_code.split(',')[0]
    if load:
        if xc != 'RPA' and self.nspin != 1:
            raise RuntimeError('not sure it would work')

        params = {}
        keys = ["kernel_fname", "kernel_format", "kernel_path_hdf5"]
        for key in keys:
            if key in kw.keys():
                params[key] = kw[key]

        kernel, kernel_dim = load_kernel(self.nprod, self.dtype, **params)

    else:
        # Lower Triangular
        kernel, kernel_dim = self.pb.comp_coulomb_pack(dtype=self.dtype)

    assert self.nprod == kernel_dim, "{} {}".format(self.nprod, kernel_dim)

    if self.nspin == 1:
        ss2kernel = [[kernel]]
    elif self.nspin == 2:
        ss2kernel = [[kernel, kernel],
                     [kernel, kernel]]
   
    # List of POINTERS !!!
    # of kernel [[(up,up), (up,dw)], [(dw,up), (dw,dw)]] TAKE CARE!!!
    if xc == 'RPA' or xc == 'HF': 
       pass

    elif xc == 'LDA' or xc == 'GGA': 
        if self.nspin == 1:
            self.comp_fxc_pack(kernel=kernel, **kw)
        elif self.nspin == 2:
            kkk = self.comp_fxc_pack(**kw) + kernel
            ss2kernel = [[kkk[0], kkk[1]], [kkk[1],kkk[2]]]
            for spin1 in range(self.nspin):
                for spin2 in range(self.nspin):
                    assert ss2kernel[spin1][spin2].dtype == self.dtype

    else:
        print(' xc_code', xc_code, xc, xc_code.split(','))
        raise RuntimeError('unkn xc_code')

    return kernel, kernel_dim, ss2kernel

def load_kernel(nprod, dtype, kernel_fname="", kernel_format="npy",
                kernel_path_hdf5=""):
    """
    Loads kernel from file and initializes field... Useful? Rewrite?

    Inputs:

        nprod (int)
        dtype (numpy data type)
        kernel_fname (string)
        kernel_format (optional, string)
        kernel_path_hdf5 (optional, string)

    Outputs:
    
        kernel
        kernel_dim
    """

    if kernel_format == "npy":
        kernel = dtype(np.load(kernel_fname))
    
    elif kernel_format == "txt":
        kernel = np.loadtxt(kernel_fname, dtype=dtype)
    
    elif kernel_format == "hdf5":
        import h5py
        if not kernel_path_hdf5:
            raise ValueError("kernel_path_hdf5 not set while trying to read kernel from hdf5 file.")
        kernel = h5py.File(kernel_fname, "r")[kernel_path_hdf5].value
    else:
        raise ValueError("Wrong format for loading kernel, must be: npy, txt or hdf5, got " \
                         + kernel_format)

    if len(kernel.shape) > 1:
        raise ValueError("The kernel must be saved in packed format in order to be loaded!")
  
    assert nprod*(nprod+1)//2 == kernel.size, \
          "wrong size for loaded kernel: %r %r "%(nprod*(nprod+1)//2, kernel.size)
    kernel_dim = nprod

    return kernel, kernel_dim
