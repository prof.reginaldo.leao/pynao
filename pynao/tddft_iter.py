from __future__ import print_function, division
from copy import copy
import numpy as np

from pynao.chi0_matvec import chi0_matvec
from pynao.m_chi0_noxv import chi0_mv_gpu, chi0_mv
from pynao.m_kernel_utils import kernel_initialization

class tddft_iter(chi0_matvec):
    """tddft_iter object.

    Iterative TDDFT a la PK, DF, OC JCTC

    Input Parameters:

    load_kernel: bool
                 Load the kernel from a previous calculation

    References:

    1. M. Barbry, “Plasmons in nanoparticles: Atomistic ab initio theory
       for large systems, Ph.D. thesis, University of Basque Country,
       Donostia-San Sebastián, Spain, 2018,
       http://cfm.ehu.es/view/files/MArc_barbry_2-1.pdf
    2. PySCF-NAO: An efficient and flexible implementation of linear response
       time-dependent density functional theory with numerical atomic orbitals,
       P. Koval, M. Barbry and D. Sanchez-Portal, Computer Physics Communications,
       15 2019, `10.1016/j.cpc.2018.08.004 <https://www.sciencedirect.com/science/article/pii/S0010465518302996>`_
    3. P. Koval, D. Foerster and O. Coulaud, J. Chem. Theory Comput. 6, 2654 (2010)
       https://pubs.acs.org/doi/abs/10.1021/ct100280x
    """

    def __init__(self, **kw):

        self.load_kernel = load_kernel = kw['load_kernel'] if 'load_kernel' in kw else False

        chi0_matvec.__init__(self, **kw)

        self.xc_code_mf = copy(self.xc_code)
        if "xc_code" in kw.keys():
            self.xc_code = kw['xc_code']
        else:
            kw['xc_code'] = self.xc_code

        if self.GPU:
            self.chi0_mv = chi0_mv_gpu
        else:
            self.chi0_mv = chi0_mv

        if not hasattr(self, 'pb'):
          print(__name__, 'no pb?')
          print(__name__, kw.keys())
          raise ValueError("product basis not initialized?")

        self.kernel, self.kernel_dim, self.ss2kernel = \
                kernel_initialization(self, **kw)

        if self.verbosity > 0:
            print(__name__,'\t====> self.xc_code:', self.xc_code)

    def comp_fxc_lil(self, **kw): 
        """
        Computes the sparse version of the TDDFT interaction kernel
        """
        from pynao.m_vxc_lil import vxc_lil
        return vxc_lil(self, deriv=2, ao_log=self.pb.prod_log, **kw)

    def comp_fxc_pack(self, **kw): 
        """
        Computes the packed version of the TDDFT interaction kernel
        """
        from pynao.m_vxc_pack import vxc_pack
        return vxc_pack(self, deriv=2, ao_log=self.pb.prod_log, **kw)

    def comp_veff(self, vext, comega=1j*0.0, x0=None):
        """
        This computes an effective field (scalar potential) given the external
        scalar potential.

        Solves
            
            Ax = b

        with

            * b the external potential delta V_ext
            * x the effective potential delta V_eff
            * A = (1 - K_{Hxc}Chi_0)
        """
        from scipy.sparse.linalg import LinearOperator

        self.matvec_ncalls = 0
        nsp = self.nspin*self.nprod
        assert len(vext) == nsp, "{} {}".format(len(vext), nsp)
        self.comega_current = comega
        veff_op = LinearOperator((nsp, nsp), matvec=self.vext2veff_matvec,
                                 dtype=self.dtypeComplex)

        # right hand side of the equation
        rhs = np.require(vext, dtype=self.dtypeComplex, requirements='C')

        #print("shape: ", veff_op.shape, rhs.shape)
        # Solves Ax = b
        resgm, info = self.krylov_solver(veff_op, rhs, x0=x0, **self.krylov_options)

        if info != 0:
            print("LGMRES Warning: info = {0}".format(info))

        return resgm

    def vext2veff_matvec(self, vin):
        dn0 = self.apply_rf0(vin, self.comega_current, self.chi0_mv)
        vcre, vcim = self.apply_kernel(dn0)
        return vin - (vcre + 1.0j*vcim)

    def vext2veff_matvec2(self, vin):
        dn0 = self.apply_rf0(vin, self.comega_current, self.chi0_mv)
        vcre,vcim = self.apply_kernel(dn0)
        return 1 - (vin - (vcre + 1.0j*vcim))

    def apply_kernel(self, dn):
        if self.nspin == 1:
            return self.apply_kernel_nspin1(dn)
        elif self.nspin == 2:
            return self.apply_kernel_nspin2(dn)

    def apply_kernel_nspin1(self, dn):

        daux  = np.zeros(self.nprod, dtype=self.dtype)
        daux[:] = np.require(dn.real, dtype=self.dtype, requirements=["A","O"])
        vcre = self.spmv(self.nprod, 1.0, self.kernel, daux)

        daux[:] = np.require(dn.imag, dtype=self.dtype, requirements=["A","O"])
        vcim = self.spmv(self.nprod, 1.0, self.kernel, daux)
        return vcre,vcim

    def apply_kernel_nspin2(self, dn):

        vcre = np.zeros((2,self.nspin,self.nprod), dtype=self.dtype)
        daux = np.zeros((self.nprod), dtype=self.dtype)
        s2dn = dn.reshape((self.nspin,self.nprod))

        for s in range(self.nspin):
            for t in range(self.nspin):
                for ireim,sreim in enumerate(('real', 'imag')):
                    daux[:] = np.require(getattr(s2dn[t], sreim),
                                         dtype=self.dtype, requirements=["A","O"])
                    vcre[ireim,s] += self.spmv(self.nprod, 1.0, self.ss2kernel[s][t], daux)

        return vcre[0].reshape(-1), vcre[1].reshape(-1)

    def comp_polariz_nonin_xx(self, comegas, tmp_fname=None):
        """
        Compute the non-interacting polarizability along the xx direction
        """
        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 0.0, 0.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)
        return self.p0_mat[0, 0, :]

    def comp_polariz_inter_xx(self, comegas, tmp_fname=None):
        """
        Compute the interacting polarizability along the xx direction
        """
        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 0.0, 0.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)
        return self.p_mat[0, 0, :]

    def comp_polariz_nonin_ave(self, comegas, tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)

        Pavg = np.zeros((self.p0_mat.shape[2]), dtype=self.dtypeComplex)
        for i in range(3):
            Pavg[:] += self.p0_mat[i, i, :]

        return Pavg/3

    def comp_polariz_inter_ave(self, comegas, tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)

        Pavg = np.zeros((self.p_mat.shape[2]), dtype=self.dtypeComplex)
        for i in range(3):
            Pavg[:] += self.p_mat[i, i, :]

        return Pavg/3
    polariz_inter_ave = comp_polariz_inter_ave

    def comp_polariz_nonin_Edir(self, comegas, Eext=np.array([1.0, 1.0, 1.0]),
                                tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas, Eext=Eext,
                                          tmp_fname=tmp_fname,
                                          inter=False)

        return self.p0_mat

    def comp_polariz_inter_Edir(self, comegas, Eext=np.array([1.0, 1.0, 1.0]),
                                tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas, Eext=Eext,
                                          tmp_fname=tmp_fname,
                                          inter=True)

        return self.p_mat
