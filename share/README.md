# Collection of resources related to PyNAO

## basis

Pasis-set collection used for the tests

## docker

Folder containing the docker recipe for the CI on gitlab

## failing_tests

Tests from PySCF-NAO that are broken

## pseudo

Collection of PseudoPotential for Siesta calculations

## siesta-arch.make

Some optimized arch.make for Siesta compilation

## singularity-img

Singularity recipes
