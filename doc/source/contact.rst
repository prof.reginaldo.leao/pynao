.. _contact:

=======
Contact
=======

If you have any question, you can contact the mainteners of the code:

* Marc Barbry marc.barbry@mailoo.org
* Peter Koval koval.peter@gmail.com

Issues
======

If you encounter any issue, feel free to write us an email or to open an issue
on the Gitlab `repository <https://gitlab.com/mbarbry/pynao/-/issues>`_
