.. _gwa:

================================================
GW Approximation and its implementation in PyNAO
================================================

Theory - Formalism
================

The GW approximation (GWA) has been shown to be a reliable method in order to
obtain quasiparticle (QP) energies as well as many other physical properties of
materials. Correspondingly, its formalism has been extensively presented in
detail elsewhere. In particular, we refer users to read the recent paper of
Koval P. et al. [koval2019GW]_ which extensively describes the implementation of
:math:`G_0W_0` calculations in `MBPT_LCAO` package.

Hamiltonian
-----------

In principle, GWA is derived from Many Body Perturbation Theory (MBPT) [Fetter1971]_.
The Green's function theory is a proper way to investigate single-particle
excitation QP energies [galitskii1958]_, [aryasetiawan1998gw]_.
Accordingly, the generalized Hedin's GW formulation starts with the interacting
Green's function :math:`G_{\sigma}(\omega)` which can be obtained in spectral
representation as follows

.. math::

    G_{\sigma}(r,r^{'},\omega) = \sum_{i}\frac{\psi_{i\sigma }(r)
    \psi_{i\sigma}^{*}(r^{'})}{\omega-E_{i\sigma}-i\eta sgn(\varepsilon_{F}-E_{i\sigma})}

where :math:`\psi_{i\sigma }` and :math:`E_{i\sigma}` represent some single-particle
wavefunction and eigenenergy for each spin channel
(:math:`\sigma = \uparrow , \downarrow`).
The quantities :math:`\eta`, and :math:`\varepsilon_{F}` are a positive infinitesimal
shift from the real axis and Fermi energy, respectively. The physical interpretation
of the poles of :math:`G(\omega)` in the prior equation is the exact QP energies
for a charged system, i.e. :math:`N\pm1` particles when QP will be particularly
referred to an excitation stemming from the single-particle state [martin_GW_2016]_,
[aryasetiawan1998gw]_.
Around Fermi level, :math:`\psi_{i\sigma}` are the solutions of the following QP equation

.. math::

    H_{0}(r)\psi_{i\sigma}(r, \omega) + \int \Sigma_{\sigma} (r, r^{'}, \omega)
    \psi_{i\sigma}(r^{'}, \omega) d{r}'^3 = E_{i\sigma}(\omega) \psi_{i\sigma}(r, \omega)

where :math:`E_{i\sigma}` represents a complex value of QP energy defining QP 
intensity and its lifetime given by :math:`1/Im(E_{i\sigma}(\omega)`.
:math:`\Sigma_{\sigma} (r,r^{'},\omega)` is the spin-resolved nonlocal complex
potential known as the self-energy that arises from a first-order perturbation.
In fact, all electron-electron interaction effects beyond non-interacting particles
(Hartree potential) are included in the self-energy [Fetter1971]_, [Mattuck1976]_. 

Self-energy and Screened interaction
------------------------------------

Calculating the self-energy had remained a significant problem in condensed
matter physics for decades. In 1965, Hedin formulated a set of coupled integral
equations that determines a self-consistent relationship among the self-energy
(:math:`\Sigma`), the single-particle Green's function, and the generalized
screened Coulomb interaction (:math:`W`) [hedin1965new]_.
Correspondingly, the spin-resolved matrix element of :math:`\Sigma_{\sigma}` is
given by integral equation including the interacting single-particle Green's
function :math:`G_{\sigma}(r,r^{'}; \omega)` and :math:`W` in space-spin-time
coordinates when the product is a convolution in the frequency domain

.. math::

    \Sigma_{\sigma }(r, r^{'}; \omega )= \lim_{\eta\rightarrow 0^{+}}
    \frac{i}{2\pi}\int_{-\infty}^{+\infty } G_{\sigma}(r,r^{'};
    \omega+\omega^{'})W(r,r^{'}; \omega^{'}) e^{i\eta\omega^{'}} d\omega^{'}.

Note that the imaginary part in the exponent term originates from the
infinitesimal shift of the time argument of :math:`W` [martin_GW_2016]_.
The recent equation for zeroth-order starting point could be compacted as
:math:`\Sigma_{\sigma} = iG_{\sigma}^{0}W^{0}` justifying the nomination of the
approximation. In MBPT, :math:`G` for each spin component is given by Dyson
equation with a self-energy kernel

.. math::

    G_{\sigma}= G_{\sigma}^{0} + G_{\sigma}^{0} \Sigma_{\sigma} G_{\sigma} + ...
    = \frac{1}{{G_{\sigma}^{0}}^{-1}-\Sigma_{\sigma}}

where :math:`G^{0}_{\sigma}` is a direct propagation from :math:`r` to :math:`r^{'}`
for each spin channel without exchange-correlation interaction which is routinely
referred to as non-interacting Green's function. For a spin-independent Hamiltonian,
not including spin-orbit interaction or non-collinear magnetic states, one can
assume is that the Green's function is spin-diagonal
(:math:`G_{\sigma{\sigma}'} = \delta _{\sigma{\sigma}'} G_{\sigma}`).

On the other hand, the dynamical characteristics of :math:`W` are established by
the irreducible polarizability (`P`) as the response to the total independent-particle
potential [martin_RPAchapter12]_. This response could be obtained in random-phase
approximation (RPA) [RPA]_ as follows [VanSetten2015]_:

.. math::

    P(r,r^{'}; \omega ) = \frac{-i}{2\pi} \int_{-\infty}^{+\infty } G_{\sigma}(r,r^{'};
    \omega+\omega^{'})G_{ \sigma}(r^{'}\kern-1mm,r\kern+1mm; \omega^{'})
    e^{i\eta\omega^{'}} d\omega^{'}.

As one can see :math:`P` consists of non-interacting electron-hole pairs due to
the opposite ordering of the two Green's functions. The screened interaction then
could be obtained in terms of :math:`P` as a Dyson-like equation in the following
common shorthand notation

.. math::

    W = \upsilon+\upsilon PW

where :math:`\upsilon` represents the Coulomb potential defined as
:math:`\upsilon (r,r^{'})=\left | r-r^{'} \right |^{-1}`.
Accordingly, :math:`W` as well as :math:`\upsilon` in RPA is spin-independent
and :math:`\Sigma_{\sigma }` inherits the spin-dependence entirely from
:math:`G_{\sigma}` term [aryasetiawan2009]_. If the Green function is spin-diagonal
for a spin-independent Hamiltonian, as mentioned above, both :math:`P` and 
:math:`\Sigma` will be recovered in the original Hedin's equations [martin_RPAchapter12]_,
[aryasetiawan2008]_. Therefore, :math:`W` as the total perturbation of the
electrostatic potential in response to the density change could be obtained as

.. math::

    W(r,r^{'}; \omega )= \left [\delta (r,r^{'''}) - \upsilon(r,r^{''})P(r^{''}\kern
    -0.5mm, r^{'''}; \omega )\right ]^{-1}\upsilon(r^{'''},r{'})

Straightforwardly, the correlation part of :math:`W` which is basically defined
by :math:`W^{c} = W - \upsilon` through the use of Taylor's expansion can be
written in compact notation as

.. math::

    W^{c} = \left ( 1-\upsilon P \right )^{-1} \upsilon P \upsilon.

Now we can derive the frequency-dependent correlation part of self-energy.
In this way, a generalization of spin-dependence gwa could be written
[aryasetiawan2008]_, [aryasetiawan2009]_, [ahmed2014spin]_. For QP energies, it
has been suggested that it is not a splendid way to utilize a full-self-consistent
calculations [Koval2014]_. For instance, :math:`G_{0}W_{0}` approach as
non-self-consistent perturbative calculations will be able to calculate accurate
self-energy with the aid of a fixed :math:`W` in RPA and a non-interacting Green's
function [gui2018]_, [Blase2011]_. However, this method satisfies the dynamical
effects coming from frequency dependence but it does not allow the evaluation of
full spectral function and QP lifetime. So far, pynao is able to do this approximation.
We can derive the correction of QP energy :math:`E_{i\sigma}^{QP}` in the
first-order approximation for a mean-field eigenvalue (:math:`E_{i\sigma}^{0}`) as

.. math::

    E_{i\sigma}^{QP} = E_{i\sigma}^{0}+\sum_{\sigma} \left \langle \psi_{i\sigma}\left|
    \Sigma_{\sigma}^{c} (E_{i\sigma}^{QP})+\Sigma_{\sigma}^{x} - V_{\sigma}^{xc}
    \right|  \psi_{i\sigma}\right \rangle
 
where self-energy operator following the convention in literature is splitted
into energy-dependent :math:`\Sigma_{\sigma}^{c} (E_{i\sigma}^{QP})` and
bare-exchange Fock operator  :math:`\Sigma_{\sigma}^{x}` parts and :math:`V_{xc}`
represents the xc potential approximated at the mean-field level. 
The previous equation could be solved at least in two different approaches:

* numerically evaluated through the instrumentality of Z-factor
* an iterative procedure.

The latter is performed in the current implementation and will be discussed in detail.

Attention must be taken when HF was applied as a starting point, the Hamiltonian
is already included the Fock operator. In order to prevent double-counting, therefore,
only correlation part of self-energy :math:`\Sigma_{\sigma}^{c}` needs to be taken
into account.

Numerical method: Frequency convolution integral in :math:`\Sigma^{c}_{\sigma}`
-------------------------------------------------------------------------------

It is well known that the calculation of :math:`\Sigma^{c}` is the most challenging
part in gwa due to the frequency convolution integral. There are at least three
approaches to deal with frequency dependence of :math:`\Sigma^c (\omega)`:

* analytic continuation from the imaginary to the real frequency axis,
* the frequency dependence of the dielectric function by a plasmon pole model
  (PPM) assuming that only one plasmon is excited
* contour deformation technique which avoids dealing with the pole structure
  along the real axis in :math:`\Sigma^{c}` [Godby1988]_.

The latter was implemented in PyNAO to evaluate :math:`\Sigma^{c}(\omega)` as
comes in the following.

Assuming :math:`\omega^{'}` to be a complex plane consisting of the poles of
the Green's function and :math:`W^{c}`. Based on the complex integration theorem,
the integral over the real frequency axis can be transformed into an integration
along the imaginary axis over the contour (:math:`\Gamma`) plus a summation over
the residues of the Green's function poles as the singularity lying inside the
:math:`\Gamma` in the first quadrant (See Fig. 1) [complexcontour]_, [Lebegue2003]_.
Note that the poles of :math:`W^{c}({\omega}')` are static in the integration
while the poles of :math:`G^{0}(\omega+{\omega}')` shift depending on where
:math:`\Sigma^{c}` will be evaluated. Therefore, the correlation self-energy
operator in the zeroth order could be computed self-consistently over the
frequency argument (:math:`\omega`) by the following equation

.. math::

    \Sigma_{\sigma}^{c} (r, r', \omega) = \frac{-1}{2\pi}\int_{-\infty }^{+\infty}
    G_{\sigma}^{0}(r, r', \omega+i{\omega}')W^{c}(r, {r}',i{\omega}')d{\omega}' +
    \sum_{\Lambda (\omega)\in \Gamma} f_{i}\Psi _{i\sigma}(r)W^{c}(r, {r}',
    i\Lambda_{i}(\omega))\Psi_{i\sigma}({r}')

in this equation, the summation is restricted to :math:`\Lambda (\omega)` denoting
a set of poles located inside the contour :math:`\Gamma` depending on their frequencies.
In order to release the frequency dependence, we performed a discretizing along
the imaginary-frequency axis with a modest number of points based on the logarithmic
numerical grid method [talman2009numsbt]_. As might be expected the integration
typically achieves the convergence quickly due to the sampled number of points
in the logarithmic mesh. On the contrary, the residues contributed by the Green's
function must be computed at the complex poles with frequency dependence making
it more demanding.

.. image:: images/contour2.png
   :scale: 50 %
   :alt: Image not found
Fig 1. The contour deformation method for computing :math:`\Sigma^{c}` over the
contour :math:`\Gamma`. IP, :math:`\mu` and EA represent ionization potential,
chemical potential and electron affinity, respectively.

Basis set: Technical Execution of NAOs
--------------------------------------

The choice of basis set fully depends on the aim of calculation. The basis set
could either be consisted of *localized atomic orbitals* or *plane waves*.
In PyNAO, we have opted for the spatially localized basis sets for the following
reasons:

* First, it is widely accepted that simulation of materials containing localized
  states with *d*- or *f*-electrons with a finite set of basis functions such as
  Gaussian, Slater or numerical atomic orbitals (NAOs) are much more appropriate,
  while plane-wave basis sets in such conditions are extremely expensive [aryasetiawan1998gw]_.
* Second, having established that plane-waves need periodic boundary conditions,
  the achievement of convergence for molecules placing in a supercell including
  the vacuum gap is computationally impractical [Freysoldt2008]_.

*Linear combination of atomic orbital* (LCAO) has been gaining importance in the
recent decade not only for enhancing *O(N)* methods of the eigenvalue equation
but also for forming more efficient basis sets with the high degree of accuracy
for localized orbitals [Ozaki2004]_.
LCAO is an effective way to improve the applicability of electronic structure methods
for evolving to the large systems. Therefore, to exploit these advantages, we
performed LCAO for defining molecular orbitals and their products when single-particle
wave functions in this framework could be expanded into coefficients
(:math:`X_{a}^{i}`) and atomic orbitals (:math:`\Phi^{a} (r)`) as follows

.. math::

    \psi_{i}(r) = \sum_{i} X_{a}^{i} \Phi^{a} (r-R_{a})

here :math:`\Phi^{a} (r)` could be decomposed into radial :math:`f^{a}(r)` and
spherical harmonic :math:`Y(\theta, \phi)` segments being function of distance
and direction of :math:`r`, respectively.  For representing :math:`f^{a}(r)`,
NAOs which are more flexible in this respect have been employed. In NAO, the
accuracy and computational efficiency directly depend on the size, range, and
radial shape of the atomic orbitals when accuracy can be maximized by optimizing
the shape of the atomic orbitals [nao2001]_. Particularly in gwa, the feasibility
of using the product of NAOs is a very effective method [Foerster2011]_.
Fock operator is a sophisticated example that was recently noted by Koval P. et
al. [koval2019GW]_. To illustrate the performance of a spatially localized basis set
made of NAOs, we turn our attention to the central elements of the gwa, i.e. full
electron-hole matrix elements of the screened interaction :math:`I(\omega)`.
Traditionally, It can be computed for occupied states :math:`(n, m)` by the
following way

.. math::

    I^{nm\sigma} (\omega) = \sum_{\sigma }\iint \psi_{n\sigma}^{*}(r)
    \psi_{m\sigma}(r) W_{c}(r,r^{'}, \omega) \psi_{n\sigma}(r^{'})
    \psi_{m\sigma}^{*}(r^{'}) dr dr^{'}  

From the previous equations above one can summarize the :math:`GW` correction in
the form of bellow

.. math::

    E_{n, \sigma} = E_{n,\sigma}^{0} - \frac{1}{2\pi}\int_{-\infty}^{+\infty}
    \sum_{m}\frac{I^{nm\sigma}(i{\omega}')}{E_{n,\sigma}+i{\omega}' - E_{m,\sigma}^0}
    d{\omega}' + \sum_{z_k(E_n)\in \Gamma} {f_k}I^{nk\sigma} (z_k(E_{n,\sigma})).

In the :math:`G_0W_0` method as implemented in PyNAO, :math:`E_{n, \sigma}` will
be iteratively corrected to achieve the convergency threshold.  In this case,
:math:`I^{nm\sigma}(i{\omega}')` needs to be calculated once. Despite, this is
the bottleneck of :math:`GW` calculations and demands a huge amount of memory
and processing. On the contrary, :math:`I^{nk\sigma} (z_k(E_{n,\sigma}))` in the
vicinity of the real frequency axis has to be repeated in each the iteration
because of the dependency of :math:`z_k(E_n)` on the QP eigenenergy :math:`E_n`.
In PyNAO, we coded an iterative procedure using the advantages of NAOs to overcome
the complexity of this level of calculations. So that by inserting *LCAO* and
expansion of NAO's products :math:`(f^{a}(r)f^{b}(r))` into the recent
multi-centered integral, the latter equation will lead to an :math:`O(N^{4})` operations.

At this point to have efficient performance in :math:`O(N^{3})` scaling, a local
basis of *dominant products* :math:`(F^{\mu}(r))` was constructed being related
to NAO products via *product vertex* :math:`(V_{\mu}^{ab})` as follows [foerster2008]_

.. math::

    f^{a}(r)f^{b}(r)=\sum_{\mu} V_{\mu}^{ab} F^{\mu}(r)

This *product vertex* as a set of coefficients is a sparse matrix with nonzero
elements if and only if all :math:`a, b, \mu` belong to the same atom pair [talman1984numerical]_.
Consequently, :math:`I(\omega)` takes the following tensorial form

.. math::

    I^{\sigma nm} (\omega) = \frac{-1}{2\pi} \sum_{\sigma} \sum_{n,m} \:
    V ^{\sigma n m}_\mu \:\:\: W_{c}^{\mu\nu}  \:\: V ^{\sigma m n}_\nu
 
where we introduced the product vertex between molecular orbitals as follows

.. math::

    V^{\sigma n m}_\mu = X_{a}^{\sigma n} V_{\mu}^{ab}X_{b}^{\sigma m}

here :math:`X_{a}^{\sigma n}` and :math:`X_{b}^{\sigma m}` are the corresponding
coefficients of occupied states :math:`(n, m)` for each spin state :math:`\sigma`
where :math:`a` and :math:`b` represents which atomic orbitals are combined in
the term. Similarly, the real space and tensorial equations of matrix elements
of non-interacting irreducible polarizability :math:`(P^{0})` yields

.. math::

    P^{0}(r, {r}', \omega)=\sum_{\sigma} \iint \frac{\Psi_{\sigma n}^{*}(r)
    \Psi_{\sigma m}(r)\Psi_{\sigma m}^{*}(r')\Psi_{\sigma n}(r')}
    {\omega - E_{\sigma m} + E_{\sigma n}+i\eta}drd{r}'

    P_{\mu\nu}^{0}=\sum_{\sigma}\sum_{n,m} (f_{n}-f_{m}) \frac{ \:\: X_{a}^{\sigma n}
    V_{\mu}^{ab}X_{b}^{\sigma m} \:\: X_{a}^{\sigma n}V_{\nu}^{ab}X_{b}^{\sigma m}}
    {\omega-E_{\sigma m}+ E_{\sigma n}}.

Eventually, the correlation part of RPA-:math:`W` in the tensorial format along
real-frequency axes could be computed by the following equation

.. math::

    W_{c}^{\mu\nu} = \left [ \delta_{\nu^{'}}^{\mu}-\upsilon^{\mu \mu^{'}}
    P_{\mu^{'} \! \nu^{'}}^{0} (\omega) \right ]^{-1} \:\: \upsilon^{\nu{'}
    \! \mu^{''}} P_{\mu^{''} \! \nu^{''}}^{0} (\omega) \: \upsilon^{\nu{''} \! \nu}

where :math:`\upsilon^{\mu \mu^{'}}` represents the coulumb interaction
and defined as
:math:`\upsilon ^{\mu{\mu}'} = \int F^{\mu}(r)\frac{1}{\left|r-{r}'\right |}F^{{\mu}'}({r}')drd{r}'`.
By the aim of this approach, the size of the basis will be decreased and helps
to minimize memory usage for storing the whole matrix elements of :math:`W_{c}`
that will significantly affect the computational cost.

Examples
========

Starting calculations from SIESTA
---------------------------------

After the SIESTA calculation is completed (see :ref:`Runing DFT section <DFT>`),
one can use the stored data to compute :math:`G_0W_0` correction.
The Python script needs to load the :mod:`gw_iter <pynao.gw>` class.
The loaded class can take the system label
as an argument in order to load the data from the prior SIESTA calculation.

We refer users to `examples/S2_triplet` directory to see example of input files
for SIESTA and PyNAO that calculates :math:`G_0W_0` energy levels for Disulfur
molecule (:math:`S_2`) as an open shell molecule with 2 unpaired valence electrons.

There are vast numbers of common arguments that can be kept in default values to
start a trial calculation.


>>> from pynao import gw as gw_c
>>> import os
>>> dname = os.getcwd()
>>> gw = gw_c ( label = 'S2', cd = dname, rescf = True, magnetization = 2)
>>> gw.kernel_gw()
>>> gw.report()

Using Gaussian-based HF as implemented in PySCF
-----------------------------------------------

PyNAO was enabled to do :math:`G_0W_0` correction on top of mean-field (so far
only HF) calculations done by PySCF code which performs a Gaussian type orbital
(GTO) basis set. A typical Python script to organize such a calculation for
:math:`O_2` molecule is given below. In the first block of the script, we import
the PySCF’s objects gto and SCF as well as :mod:`gw_iter <pynao.gw>` class.
Then selected molecule with given coordinates of oxygen atoms and a desire Gaussian
basis set like Dunning's basis `cc-pVQZ` was defined in the second block.
Here `SCF.UHF` does an unrestricted HF calculation and kernel computes the
corresponding energies, eigenfunctions, and so forth. The last block launches 
:math:`G_0W_0` class in the iterative scheme. At the end of this block
:mod:`gw.kernel_gw_iter <pynao.gw>` computes the :math:`G_0W_0` corrections in
energies and :mod:`report provides <pynao.gw>` a plain-text report file including
QP spectra as well as mean-field energies.

>>> from pySCF import gto, SCF
>>> from pynao import gw\_iter
>>> mol = gto.M ( atom = '''O 0.0, 0.0, 0.622978 ; O 0.0, 0.0, -0.622978''', basis = 'ccpvqz' , spin = 2)
>>> mf = SCF.UHF (mol)
>>> mf.kernel()
>>> gw = gw_iter (mf = mf, gto = mol,
>>>   verbosity = 4,
>>>   niter_max_ev = 10, tol_ev = 1e-04,
>>>   nocc = 6 , nvrt = 10,
>>>   nff_ia = 28,
>>>   write_R = True,
>>>   limited_nbnd = False,
>>>   pass_dupl = False,
>>>   use_initial_guess_ite_solver = True,
>>>   SCF_kernel_conv_tol = 1.0e-6,
>>>   dtype = np.float32,
>>>   kmat_algo = 'sm0_sum',
>>>   gw_xvx_algo= "dp_sparse",
>>>   krylov_solver= "lgmres",
>>>   krylov_options= {"tol": 1.0e-3, "atol": 1.0e-3, "outer_k": 5})
>>> gw.kernel\_gw\_iter()
>>> gw.report()


References
==========

.. [koval2019GW] Peter Koval, Mathias Per Ljungberg, Moritz Müller, and Daniel
   Sánchez-Portal. Toward efficient gw calculations using numerical atomic orbitals:
   Benchmarking and application to molecular dynamics simulations. J. Chem. Theory
   Comput, 15(8):4564–4580, 2019
.. [Fetter1971] A.L. Fetter and J.D. Walecka. Quantum Theory of Many-Particle
   Systems. McGraw-Hill, New York, 1971.
.. [galitskii1958] F Aryasetiawan and O Gunnarsson. The GW method. Reports on
   Progress in Physics, 61(3):237–312, mar 1998.
.. [aryasetiawan1998gw] Viktor Mikhailovich Galitskii and Arkadii Beinusovich
   Migdal. Application of quantum field theory methods to the many body problem.
   Sov. Phys. JETP, 7(96), 1958.
.. [martin_GW_2016] Richard M. Martin, Lucia Reining, and David M. Ceperley.
   Interacting Electrons: Theory and Computational Approaches. Cambridge
   University Press, 2016.
.. [Mattuck1976] R.D. Mattuck. A Guide to Feynman Diagrams in the Many-body Problem.
   Dover Books on Physics Series. Dover Publications, Incorporated, 1976.
.. [hedin1965new] Lars Hedin. New method for calculating the one-particle Green’s
   function with application to the electron-gas problem. Physical Review, 139(3A):A796, 1965
.. [martin_RPAchapter12] Richard M. Martin, Lucia Reining, and David M. Ceperley.
   The RPA and the GW approximation for the self-energy, pages 245–279.
   Cambridge University Press, 2016.
.. [RPA] David Pines and David Bohm. A collective description of electron
   interactions: Ii. collective vs individual particle aspects of the interactions.
   Phys. Rev., 85:338–353, Jan 1952.
.. [VanSetten2015] Michiel J. Van Setten, et al. GW100: Benchmarking G0W0 for
   Molecular Systems. J. Chem. Theory Comput., 11(12):5665–5687, 2015.
.. [aryasetiawan2009] Ferdi Aryasetiawan and Silke Biermann. Generalized Hedin
   equations and σGσW approximation for quantum many-body systems with spin-dependent
   interactions. Journal of Physics: Condensed Matter, 21(6):064232, jan 2009
.. [aryasetiawan2008] F. Aryasetiawan and S. Biermann. Generalized hedin’s equations
   for quantum many-body systems with spin-dependent interactions. Physical
   review letters, 100:116402, Mar 2008.
.. [ahmed2014spin] Towfiq Ahmed, Robert C Albers, Alexander V Balatsky, Christoph
   Friedrich, and Jian-Xin Zhu. GW quasi-particle calculations with spin-orbit
   coupling for the light actinides. Physical Review B, 89(3):035104, 2014.
.. [Koval2014] Peter Koval, Dietrich Foerster, and Daniel Sánchez-Portal. Fully
   self-consistent GW and quasiparticle self-consistent GW for molecules.
   Phys. Rev. B, 89(15):155417, apr 2014.
.. [gui2018] Xin Gui, Christof Holzer, and Wim Klopper. Accuracy assessment of
   GW starting points for calculating molecular excitation energies using the
   Bethe–Salpeter formalism. Journal of chemical theory and computation,
   14(4):2127–2136, 2018.
.. [Blase2011] X. Blase, C. Attaccalite, and V. Olevano. First-principles GW
   calculations for fullerenes, porphyrins, phtalo-cyanine, and other molecules
   of interest for organic photovoltaic applications. Phys. Rev. B - Condens.
   Matter Mater. Phys., 83(11):1–9, 2011.
.. [Godby1988] R. W. Godby, M. Schlüter, and L. J. Sham. Self-energy operators
   and exchange-correlation potentials in semiconductors. Phys. Rev. B,
   37(17):10159–10175, jun 1988.
.. [complexcontour] Donald J Newman and Joseph Bak. Complex analysis. Springer,
   2010.
.. [Lebegue2003] S. Lebgue, B. Arnaud, M. Alouani, and P. E. Bloechl.
   Implementation of an all-electron GW approximation based on the projector
   augmented wave method without plasmon pole approximation: Application to si,
   SiC, AlAs, InAs, NaH, and KH. Phys. Rev. B, 67(15):155208, April 2003.
.. [talman2009numsbt] JD Talman. NumSBT: A subroutine for calculating spherical
   Bessel transforms numerically. Computer Physics Communications, 180(2):332–338,
   2009.
.. [Freysoldt2008] Christoph Freysoldt, Philipp Eggert, Patrick Rinke, Arno Schindlmayr,
   and Matthias Scheffler. Screening in two dimensions: gw calculations for surfaces
   and thin films using the repeated-slab approach. Phys. Rev. B, 77:235428, Jun 2008.
.. [Ozaki2004] T. Ozaki and H. Kino. Numerical atomic basis orbitals from H to
   Kr. Phys. Rev. B, 69(19):195113, may 2004.
.. [nao2001] Javier Junquera, Óscar Paz, Daniel Sánchez-Portal, and Emilio Artacho.
   Numerical atomic orbitals for linear-scaling calculations. Phys. Rev. B,
   64(23):235111, nov 2001.
.. [Foerster2011] Dietrich Foerster, Peter Koval, and Daniel Sánchez-Portal.
   An O( N3 ) implementation of Hedin’s GW approximation for molecules. J. Chem.
   Phys., 135(7):074105, aug 2011.
.. [foerster2008] Dietrich Foerster. Elimination, in electronic structure
   calculations, of redundant orbital products. The Journal of chemical physics,
   128(3):034108, 2008. 
.. [talman1984numerical] James D Talman. Numerical calculation of four-center
   coulomb integrals. The Journal of chemical physics, 80(5):2000–2008, 1984.
