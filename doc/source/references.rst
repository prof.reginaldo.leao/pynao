.. _references:

==========
References
==========

Please, cite the following papers when using PyNAO

PyNAO and method papers
=======================

PySCF-NAO: An efficient and flexible implementation of linear response
time-dependent density functional theory with numerical atomic orbitals,
P. Koval, M. Barbry and D. Sanchez-Portal, Computer Physics Communications,
2019,
`10.1016/j.cpc.2018.08.004 <https://www.sciencedirect.com/science/article/pii/S0010465518302996>`_

A Parallel Iterative Method for Computing Molecular Absorption Spectra,
P. Koval, D. Foerster, and O. Coulaud, J. Chem. Theo. Comput. 2010,
`10.1021/ct100280x <https://pubs.acs.org/doi/abs/10.1021/ct100280x>`_

PySCF papers
============

PyNAO was originally part of the PySCF program package and currently dependent on it.
Therefore, PySCF papers must be included when using PyNAO for your work.


Recent developments in the PySCF program package,
Qiming Sun
Xing Zhang
Samragni Banerjee
Peng Bao
Marc Barbry
Nick S. Blunt
Nikolay A. Bogdanov
George H. Booth
Jia Chen
Zhi-Hao Cui
Janus J. Eriksen
Yang Gao
Sheng Guo
Jan Hermann
Matthew R. Hermes
Kevin Koh
Peter Koval
Susi Lehtola
Zhendong Li
Junzi Liu
Narbe Mardirossian
James D. McClain
Mario Motta
Bastien Mussard
Hung Q. Pham
Artem Pulkin
Wirawan Purwanto
Paul J. Robinson
Enrico Ronca
Elvira R. Sayfutyarova
Maximilian Scheurer
Henry F. Schurkus
James E. T. Smith
Chong Sun
Shi-Ning Sun
Shiv Upadhyay
Lucas K. Wagner
Xiao Wang
Alec White
James Daniel Whitfield
Mark J. Williamson
Sebastian Wouters
Jun Yang
Jason M. Yu
Tianyu Zhu
Timothy C. Berkelbach
Sandeep Sharma
Alexander Yu. Sokolov
Garnet Kin-Lic Chan
(2020), The Journal of Chemical Physics, 153, 2,
`doi: 10.1063/5.0006074 <https://aip.scitation.org/doi/10.1063/5.0006074>`_

PySCF: the Python-based Simulations of Chemistry Framework, Q. Sun, T. C. Berkelbach,
N. S. Blunt, G. H. Booth, S. Guo, Z. Li, J. Liu, J. McClain, E. R. Sayfutyarova,
S. Sharma, S. Wouters, G. K.-L. Chan (2018), PySCF: the Python‐based simulations
of chemistry framework. WIREs Comput. Mol. Sci., 8,
`doi:10.1002/wcms.1340 <https://onlinelibrary.wiley.com/doi/abs/10.1002/wcms.1340>`_

Others
======

The methods for TDDFT has been extensively described in M. Barbry Ph.D thesis

`Plasmons in Nanoparticles: Atomistic Ab Initio Theory for
Large Systems <https://cfm.ehu.es/cfm_news/phd-thesis-defense-marc-barbry/>`_, M. Barbry
