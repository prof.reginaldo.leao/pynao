.. _DFT:

=======================
Runing DFT calculations
=======================

Before to be able to be able to calculate the excited states properties of a
system with PyNAO, the gtound state properties must be calculated.


Runing Siesta calculations
--------------------------

Note that the option *coop.write* in siesta input exports the Hamiltonian and the
KS eigenstates to the files with *HSX* and *WFSX* extensions, respectively.
The latter store files are essential to start post-processing calculations.
The corresponding Python script needs to load the
:mod:`gw_iter <pynao.gw>` class. The loaded class can take the system label
as an argument in order to load the data from the prior SIESTA calculation.
We refer users to `examples/S2_triplet` directory to see example of input files
for SIESTA and PyNAO that calculates :math:`G_0W_0` energy levels for Disulfur
molecule (:math:`S_2`) as an open shell molecule with 2 unpaired valence electrons.

Runing Siesta with ASE
----------------------

Runing PySCF calculations
-------------------------
