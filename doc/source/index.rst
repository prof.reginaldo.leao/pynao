.. PyNAO documentation master file, created by
   sphinx-quickstart on Sat Aug 15 10:33:46 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================
Welcome to PyNAO's documentation
================================

PyNAO the Python Numerical Atomic Orbitals.

PyNAO is designed to perform efficient many-body perturbation theory methods
with numerical atomic orbitals. Actual implemented methods are

* TDDFT (light and electronic perturbation, i.e., EELS)
* G0W0 (GW is ready ??)
* BSE

The code needs inputs from previous DFT calculations. Supported DFT codes are

* `PySCF <https://sunqm.github.io/pyscf/>`_
* `Siesta <https://departments.icmab.es/leem/siesta/>`_

Known issues
============

See list of issues in the `Gitlab repository <https://gitlab.com/mbarbry/pynao/-/issues>`_

History
=======

PyNAO was originally developed as a branch of the PySCF package under the name
`PySCF-NAO <https://github.com/cfm-mpc/pyscf/tree/nao2>`_, and before this,
it was a Fortran code called `MBPT_LCAO <http://mbpt-domiprod.wikidot.com/>`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   gwa
   DFT
   references
   modules
   contact


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
