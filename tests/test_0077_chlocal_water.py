from __future__ import print_function, division
import unittest
from pynao import nao as nao_c
from pynao import mf as mf_c

class KnowValues(unittest.TestCase):

    def test_0077_chlocal_H2O(self):
        """
        Test chlocal field (density of bare atoms)
        """
        import os
        dname = os.path.dirname(os.path.abspath(__file__))
        nao = nao_c(label='water', cd=dname)
        g = nao.build_3dgrid_ae(level=3)
        int_chlocal = (g.weights*nao.vna(g.coords, sp2v=nao.ao_log.sp2chlocal)).sum()
        self.assertAlmostEqual(int_chlocal, -7.9999819496898787)

    def test_0077_DUscf_H2O(self):
        """
        Test chlocal field (density of bare atoms)
        """
        import os
        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(label='water', cd=dname)
        g = mf.mesh3d.get_3dgrid()
        dens = mf.dens_elec(g.coords, mf.make_rdm1()).reshape(mf.mesh3d.shape)
        dens += mf.vna(g.coords,sp2v=mf.ao_log.sp2chlocal).reshape(g.shape)
        vh = mf.vhartree_pbc(dens)

        dens1 = mf.dens_elec(g.coords, mf.make_rdm1()).reshape(mf.mesh3d.shape)
        vh1 = mf.vhartree_pbc(dens)

        dens2 = mf.vna(g.coords,sp2v=mf.ao_log.sp2chlocal).reshape(g.shape)
        vh2 = mf.vhartree_pbc(dens)

if __name__ == "__main__":
    unittest.main()
