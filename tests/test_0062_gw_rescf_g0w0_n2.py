from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import gw as gw_c

class KnowValues(unittest.TestCase):

    def test_rescf(self):
        """
        Hartree-Fock than G0W0 N2 example is marked with level-ordering change
        """
        dname = os.path.dirname(os.path.abspath(__file__))
        gw = gw_c(label='n2', cd=dname, verbosity=0, jcutoff=9, nff_ia=64,
                  tol_ia=1e-6, rescf=True) 
        gw.kernel_gw()

        ref = np.loadtxt("test_0062_gw_rescf_g0w0_n2_data.txt-ref")
        for e, eref in zip(gw.mo_energy_gw[0,0,:], ref):
            self.assertAlmostEqual(e, eref)

if __name__ == "__main__":
    unittest.main()
