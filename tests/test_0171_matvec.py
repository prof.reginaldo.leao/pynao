from __future__ import print_function, division
from timeit import default_timer as timer
import os, unittest
import numpy as np
import scipy.sparse as sp
from pynao.m_sparsetools import csr_matvec

class KnowValues(unittest.TestCase):

    def test_matvec(self, N=10, density=1.0e-1):
        """
        This is a test for the custom csr_matvec operation implemented in
        pynao.m_sparsetools. It perform a sparse matrix vector operation
        in parallel, while scipy version is serial
        """

        A = sp.random(N, N, density=density, format='csr')
        x = np.random.random(N)

        ref = A.dot(x)
        new = csr_matvec(A, x)
        assert np.mean(abs(ref - new)) < 1.0e-12

    def test_check_performances(self, N=10, density=1.0e-1):
        """
        Check the performances of parallel csr_matvec compared to serial
        scipy sparse matvec

        On a AMD Ryzen 5 2600 

                        Total time          Mean time over Ncall
        N =  10
        time scipy:  0.00043855095282197 4.3855095282197e-06
        time pynao:  0.00044646195601671934 4.4646195601671935e-06
        N =  100
        time scipy:  0.0005069759790785611 5.0697597907856105e-06
        time pynao:  0.00055560318287462 5.556031828746199e-06
        N =  1000
        time scipy:  0.007937719812616706 7.937719812616705e-05
        time pynao:  0.007971814542543143 7.971814542543143e-05
        N =  40000
        time scipy:  15.305552606121637 0.15305552606121636
        time pynao:  10.342078993970063 0.10342078993970062
        """

        skip = True
        if skip:
            return

        Ncall = 100
        for dim in [10, 100, 1000, 40000]:
            print("N = ", dim)
            A = sp.random(dim, dim, density=density, format='csr')
            x = np.random.random(dim)

            # preallocate the result vector
            y = np.zeros((dim), dtype=x.dtype)

            tsc = 0.0
            for i in range(Ncall):
                t1 = timer()
                ref = A.dot(x)
                t2 = timer()
                tsc += t2 - t1
            print("time scipy: ", tsc, tsc/Ncall)

            tpyn = 0.0
            for i in range(Ncall):
                t1 = timer()
                new = csr_matvec(A, x, y=y)
                t2 = timer()
                tpyn += t2 - t1
            print("time pynao: ", tpyn, tpyn/Ncall)

if __name__ == "__main__":
    unittest.main()
