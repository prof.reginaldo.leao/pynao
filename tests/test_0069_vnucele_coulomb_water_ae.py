from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf as mf_c
from pyscf import gto, scf

class KnowValues(unittest.TestCase):

    def test_0069_vnucele_coulomb_water_ae(self):
        """
        This
        """
        mol = gto.M(verbose=1, atom='O 0 0 0; H 0 0.489 1.074; H 0 0.489 -1.074',
                    basis='cc-pvdz')
        gto_mf = scf.RHF(mol)
        gto_mf.kernel()
        vne = mol.intor_symmetric('int1e_nuc')
        dm_gto = gto_mf.make_rdm1()
        E_ne_gto = (vne*dm_gto).sum()*0.5
        self.assertAlmostEqual(E_ne_gto, -97.67612964579993)

        tk = mol.intor_symmetric('int1e_kin')
        E_kin_gto = (tk*dm_gto).sum()
        self.assertAlmostEqual(E_kin_gto, 75.37551418889902)

        mf = mf_c(gto=mol, mf=gto_mf)
        vne_nao = mf.vnucele_coo_coulomb().toarray()
        dm_nao = mf.make_rdm1().reshape((mf.norbs, mf.norbs))
        E_ne_nao = (vne_nao*dm_nao).sum()*0.5
        self.assertAlmostEqual(E_ne_nao, -97.67612893873279)

        tk_nao = -0.5*mf.laplace_coo().toarray()
        E_kin_nao = (tk_nao*dm_nao).sum()
        self.assertAlmostEqual(E_kin_nao, 75.37551418889902)

if __name__ == "__main__":
    unittest.main()
