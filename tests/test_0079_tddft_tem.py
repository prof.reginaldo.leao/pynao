from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import tddft_tem

# fmax = 5.0 eV
fmax = 0.18374661237478318
dw = fmax/50

freq = np.arange(0.0, fmax, dw)

dname = os.path.dirname(os.path.abspath(__file__))
td = tddft_tem(label='water', cd=dname, jcutoff=7, iter_broadening=1e-2,
                xc_code='RPA', freq=freq)

class KnowValues(unittest.TestCase):

    def test_non_inter_polariz(self):
        """
        This is non-interacting polarizability TDDFT with SIESTA starting point
        """

        spec = td.get_EELS_spectrum(beam_offset=np.array([0.0, 0.0, 5.0]),
                                    velec=np.array([75.0, 0.0, 0.0]), inter=False)
        #np.save("{}/water.tddft_tem_nonin-ref.npy".format(dname), spec)
        data_ref = np.load(dname+'/water.tddft_tem_nonin-ref.npy')
        self.assertTrue(np.allclose(data_ref.real, spec.real, rtol=1.0, atol=1e-05))
        self.assertTrue(np.allclose(data_ref.imag, spec.imag, rtol=1.0, atol=1e-05))

    def test_inter_polariz(self):
        """
        This is interacting polarizability with SIESTA starting point
        """
        spec = td.get_EELS_spectrum(beam_offset=np.array([0.0, 0.0, 5.0]),
                                    velec=np.array([75.0, 0.0, 0.0]), inter=True)
        #np.save("{}/water.tddft_tem_inter-ref.npy".format(dname), spec)
        data_ref = np.load(dname+'/water.tddft_tem_inter-ref.npy')
        self.assertTrue(np.allclose(data_ref.real, spec.real, rtol=1.0, atol=1e-05))
        self.assertTrue(np.allclose(data_ref.imag, spec.imag, rtol=1.0, atol=1e-05))

if __name__ == "__main__":
    unittest.main()
