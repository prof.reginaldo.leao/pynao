from __future__ import print_function, division
import os,unittest,numpy as np
from pynao.scf import scf

class KnowValues(unittest.TestCase):

    def test_rescf(self):
        """
        reSCF
        """
        dname = os.path.dirname(os.path.abspath(__file__))
        myhf = scf(label='water', cd=dname, verbosity=0, kmat_timing=0.0,
                   kmat_algo='ac_vertex_fm')
        myhf.kernel_scf()

        #np.savetxt('test_0041_rescf_scf_mo_energy.txt-ref', myhf.mo_energy[0,0,:].T)
        ref = np.loadtxt("test_0041_rescf_scf_mo_energy.txt-ref")
        for eref, e in zip(ref, myhf.mo_energy[0,0,:]):
            self.assertAlmostEqual(eref, e)

if __name__ == "__main__":
    unittest.main()
