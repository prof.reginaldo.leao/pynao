from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf

class KnowValues(unittest.TestCase):

    def test_exc(self):
        """
        Compute exchange-correlation energy
        """
        sv = mf(label='water', cd=os.path.dirname(os.path.abspath(__file__)))
        dm = sv.make_rdm1()
        exc = sv.exc(dm, xc_code='1.0*LDA,1.0*PZ', level=4)

        # ref before 2020-08-10 (changed by merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41):
        # -4.1422239276270201
        self.assertAlmostEqual(exc, -4.142223817090067)

if __name__ == "__main__":
    unittest.main()
