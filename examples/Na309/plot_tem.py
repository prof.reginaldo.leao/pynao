import numpy as np
import matplotlib.pyplot as plt


freq = np.arange(0.0, 10.0, 0.05)
EELS = np.load("EELS_inter.npy")

h = 9
w = 4*h/3
ft = 20
lw = 4

fig = plt.figure(1, figsize=(w, h))

ax = fig.add_subplot(111)

ax.plot(freq, -EELS.imag, linewidth=lw)

ax.set_xlabel(r"Energy (eV)", fontsize=ft)
ax.set_ylabel(r"EELS (a.u.)", fontsize=ft)

fig.tight_layout()
fig.savefig("EELS.pdf", format="pdf")
