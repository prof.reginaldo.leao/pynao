"""
Run TDDFT calculations for Na309 cluster.
DFT Siesta calculations must have been succesfully performed before to
launch this script
"""
import os
import numpy as np

from pynao import tddft_iter
from ase.units import Ry, eV, Ha

# First run DFT calculation with Siesta

dname = os.getcwd() 

eps = 0.15

td = tddft_iter(label='siesta', cd=dname, verbosity=4,
                iter_broadening=eps/Ha, tol_loc=1e-4, tol_biloc=1e-6, jcutoff=7,
                level=0, xc_code='LDA,PZ',
                krylov_options={"tol": 1.0e-4, "atol": 1.0e-4})

# Calculate polarizability along x direction only
freq = np.arange(0.0, 5.0, 0.05)/Ha + 1j * td.eps
pmat = td.comp_polariz_inter_Edir(freq, Eext=np.array([1.0, 0.0, 0.0]))

np.save("Polarizability_inter.npy", pmat)
np.save("density_change_prod_basis_inter_optical.npy", td.dn)
