# GW calculation: S2 triplet

Basic example showing how to launch a GW calculation for a spin polarized system.
Ground state calculation are performed with Siesta.

    siesta < S2.fdf > S2.out

The example contains the following files

* `S2.fdf`: the siesta input file
* `S.gga.psf`: the pseudopotential definition
* `S2.py`: the pynao script to lauch the GW calculation
